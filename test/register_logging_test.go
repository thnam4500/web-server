package test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"testing"
	"time"

	"github.com/thnam4500/web-server/api"
	"github.com/thnam4500/web-server/constants"
)

type RegisterResponse struct {
	Status  int32  `json:"status"`
	Message string `json:"message"`
}

func TestRegisterLogging(t *testing.T) {
	urlSignUp := "http://localhost:3000/api/v1/signup"
	req := api.SignUpRequest{
		Username:  randomString(15),
		Password:  randomString(7),
		Email:     randomString(7) + "@gmail.com",
		FirstName: randomString(7),
		LastName:  randomString(7),
		Birthday:  1691422702,
	}
	byteArray, err := json.Marshal(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	resp, err := http.Post(urlSignUp, "application/json", bytes.NewBuffer(byteArray))
	if err != nil {
		fmt.Println(err)
		return
	}
	body, err := json.Marshal(resp.Body)
	if err != nil {
		fmt.Println(err)
		return
	}
	var registerResponse RegisterResponse
	err = json.Unmarshal(body, &registerResponse)
	if err != nil {
		fmt.Println(err)
		return
	}
	expectedRegisterResponse := RegisterResponse{
		Status:  constants.STATUS_RESPONSE_OK,
		Message: "success",
	}
	if registerResponse.Status != constants.STATUS_RESPONSE_OK {
		t.Fatal("Register is not correct")
		return
	}
	if registerResponse.Status != expectedRegisterResponse.Status {
		t.Fatal("Status is not correct")
		return
	}
	urlSignIn := "http://localhost:3000/api/v1/login"

	reqLogin := api.LoginRequest{
		Username: req.Username,
		Password: req.Password,
	}
	byteArrayLogin, err := json.Marshal(reqLogin)
	if err != nil {
		fmt.Println(err)
		return
	}
	resp, err = http.Post(urlSignIn, "application/json", bytes.NewBuffer(byteArrayLogin))
	if err != nil {
		fmt.Println(err)
		return
	}
	body, err = json.Marshal(resp.Body)
	if err != nil {
		fmt.Println(err)
	}
	var loginResponse api.LoginResponse
	err = json.Unmarshal(body, &loginResponse)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func randomString(length int) string {
	rand.Seed(time.Now().UnixNano())

	charSet := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	charSetLength := len(charSet)

	randomString := make([]byte, length)
	for i := 0; i < length; i++ {
		randomString[i] = charSet[rand.Intn(charSetLength)]
	}

	return string(randomString)
}
