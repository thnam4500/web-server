package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/thnam4500/web-server/constants"
)

type ChangePasswordRequest struct {
	OldPassword string `json:"old_password" binding:"required"`
	NewPassword string `json:"new_password" binding:"required"`
}

func (s *Server) ChangePassword(ctx *gin.Context) {
	var req ChangePasswordRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	// TODO: call internal service to change password

	ctx.JSON(http.StatusOK, gin.H{
		"status":  constants.STATUS_RESPONSE_OK,
		"message": "success",
	})
}
