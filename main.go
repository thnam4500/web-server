package main

import (
	"log"

	"github.com/redis/go-redis/v9"
	"github.com/thnam4500/web-server/api"
)

// @host      localhost:3000
// @BasePath  /api/v1
// @contact.name   Hoang Nam
// @contact.url    youtube.com
// @contact.email  ntranhoang1@gmail.com
func main() {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	server := api.NewServer(rdb)
	err := server.StartServer(":3000")
	if err != nil {
		log.Fatalln(err)
	}
}
