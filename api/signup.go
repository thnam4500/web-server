package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	identity "github.com/thnam4500/central-proto/identity-server/pb"
	"github.com/thnam4500/web-server/constants"
)

type SignUpRequest struct {
	Username  string `json:"username" binding:"required"`
	Password  string `json:"password" binding:"required"`
	Email     string `json:"email" binding:"required"`
	FirstName string `json:"first_name" binding:"required"`
	LastName  string `json:"last_name" binding:"required"`
	Birthday  int64  `json:"birthday" binding:"required"`
}

type SignUpResponse struct {
	Status int    `json:"status"`
	Error  string `json:"error"`
}

// Login godoc
// @Summary Sign up account
// @Accept  json
// @Produce  json
// @Param username body string required "username"
// @Param password body string required "password"
// @Param email body string required "email"
// @Param first_name body string required "first_name"
// @Param last_name body string required "last_name"
// @Param birthday body string required "birthday"
// @Success      200  {object}  SignUpResponse
// @Failure      400  {object}  SignUpResponse
// @Router /signup [post]
func (s *Server) SignUp(ctx *gin.Context) {
	var req SignUpRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	resp, err := s.External.IdentityClient.SignUp(ctx, &identity.SignUpRequest{
		Username:  req.Username,
		Password:  req.Password,
		Email:     req.Email,
		FirstName: req.FirstName,
		LastName:  req.LastName,
		Birthday:  req.Birthday,
	})
	if err != nil {
		ctx.JSON(http.StatusBadRequest, SignUpResponse{
			Status: constants.STATUS_RESPONSE_NOT_OK,
			Error:  err.Error(),
		})
		return
	}

	ctx.JSON(200, gin.H{
		"status":  resp.Status,
		"message": resp.Message,
	})
}
