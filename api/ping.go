package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	identity "github.com/thnam4500/central-proto/identity-server/pb"
)

// PingIdentity godoc
// @Description ping identity server
// @Sucess 200 {object} string
// @Router       /ping [get]
func (s *Server) PingIdentity(ctx *gin.Context) {
	resp, err := s.External.IdentityClient.Ping(ctx, &identity.PingRequest{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.JSON(200, gin.H{
		"status":  resp.Status,
		"message": resp.Message,
	})
}
