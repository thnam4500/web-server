package constants

const STATUS_RESPONSE_OK = 0
const STATUS_RESPONSE_NOT_OK = 1

type ErrorResponse struct {
	Status int    `json:"status"`
	Error  string `json:"error"`
}
