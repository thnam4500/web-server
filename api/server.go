package api

import (
	"github.com/gin-gonic/gin"
	"github.com/redis/go-redis/v9"
	swaggerFiles "github.com/swaggo/files"
	identity "github.com/thnam4500/central-proto/identity-server/pb"
	_ "github.com/thnam4500/web-server/docs"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	ginSwagger "github.com/swaggo/gin-swagger"
)

type Server struct {
	redisDatabase *redis.Client
	External      external
	router        *gin.Engine
}
type external struct {
	IdentityClient identity.IdentityClient
}

func NewServer(rdb *redis.Client) *Server {
	server := &Server{redisDatabase: rdb}
	conn, err := grpc.Dial("localhost:9000", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		panic(err)
	}
	server.External.IdentityClient = identity.NewIdentityClient(conn)
	router := gin.Default()
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	v1 := router.Group("/api/v1")
	{
		v1.POST("/login", server.Login)
		v1.POST("/signup", server.SignUp)
		v1.GET("/ping", server.PingIdentity)
		v1.GET("/friends/:user_id", server.FollowList)
		v1.POST("/friends/:user_id/follow", server.FollowUser)
		v1.POST("/friends/:user_id/unfollow", server.UnFollowUser)
	}
	server.router = router
	return server
}

func (s *Server) StartServer(address string) error {
	return s.router.Run(address)
}
