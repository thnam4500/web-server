package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/thnam4500/web-server/constants"
)

type ChangeEmailRequest struct {
	NewEmail string `json:"new_email" binding:"required"`
}

func (s *Server) ChangeEmail(ctx *gin.Context) {
	var req ChangeEmailRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	// TODO: call internal service to change email

	ctx.JSON(http.StatusOK, gin.H{
		"status":  constants.STATUS_RESPONSE_OK,
		"message": "success",
	})
}
