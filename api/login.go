package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	identity "github.com/thnam4500/central-proto/identity-server/pb"
	"github.com/thnam4500/web-server/constants"
)

type LoginRequest struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type LoginResponse struct {
	Status  int32  `json:"status"`
	Token   string `json:"token"`
	Message string `json:"message"`
}
type LoginErrorResponse struct {
	Status int    `json:"status"`
	Error  string `json:"error"`
}

// Login godoc
// @Summary Login account
// @Accept  json
// @Produce  json
// @Param username body string required "username"
// @Param password body string required "password"
// @Success      200  {object}  LoginResponse
// @Failure      400  {object}  LoginErrorResponse
// @Router /login [post]
func (s *Server) Login(ctx *gin.Context) {
	var req LoginRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, LoginErrorResponse{
			Status: constants.STATUS_RESPONSE_NOT_OK,
			Error:  err.Error(),
		})
		return
	}
	resp, _ := s.External.IdentityClient.Login(ctx, &identity.LoginRequest{
		Username: req.Username,
		Password: req.Password,
	})
	ctx.JSON(http.StatusOK, resp)
}
