package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	identity "github.com/thnam4500/central-proto/identity-server/pb"
	"github.com/thnam4500/web-server/constants"
)

type UnFollowUserRequest struct {
	UserId       int64 `json:"user_id" binding:"required"`
	FollowUserId int64 `json:"follow_user_id" binding:"required"`
}

func (s *Server) UnFollowUser(ctx *gin.Context) {
	var req FollowUserRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	resp, err := s.External.IdentityClient.UnFollowFriend(ctx, &identity.UnFollowFriendRequest{
		UserId:   req.UserId,
		FriendId: req.FollowUserId,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"status":  constants.STATUS_RESPONSE_NOT_OK,
			"message": "Internal Server Error",
		})
		return
	}
	if resp.Status != constants.STATUS_RESPONSE_OK {
		ctx.JSON(http.StatusOK, gin.H{
			"status":  constants.STATUS_RESPONSE_NOT_OK,
			"message": "fail",
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"status":  constants.STATUS_RESPONSE_OK,
		"message": "success",
	})
}
