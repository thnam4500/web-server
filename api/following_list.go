package api

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	identity "github.com/thnam4500/central-proto/identity-server/pb"
	"github.com/thnam4500/web-server/constants"
)

func (s *Server) FollowList(ctx *gin.Context) {
	userIdString := ctx.Param("user_id")
	pageQuery := ctx.DefaultQuery("page", "0")
	limitQuery := ctx.DefaultQuery("limit", "0")

	userId, err := strconv.ParseInt(userIdString, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	page, err := strconv.ParseInt(pageQuery, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	limit, err := strconv.ParseInt(limitQuery, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return

	}
	resp, err := s.External.IdentityClient.FollowFriendList(ctx, &identity.FollowFriendListRequest{
		UserId: userId,
		Page:   int32(page),
		Limit:  int32(limit),
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"status":  constants.STATUS_RESPONSE_NOT_OK,
			"message": "Internal Server Error",
		})
		return
	}
	if resp.Status != constants.STATUS_RESPONSE_OK {
		ctx.JSON(http.StatusOK, gin.H{
			"status":  constants.STATUS_RESPONSE_NOT_OK,
			"message": "fail",
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"status":  constants.STATUS_RESPONSE_OK,
		"message": "success",
		"data":    resp.Data,
	})
}
