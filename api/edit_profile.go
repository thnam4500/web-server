package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/thnam4500/web-server/constants"
)

type EditProfileRequest struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Birthday  int64  `json:"birthday"`
}

func (s *Server) EditProfile(ctx *gin.Context) {
	var req EditProfileRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	// TODO: call internal service to edit profile

	ctx.JSON(200, gin.H{
		"status":  constants.STATUS_RESPONSE_OK,
		"message": "success",
	})
}
